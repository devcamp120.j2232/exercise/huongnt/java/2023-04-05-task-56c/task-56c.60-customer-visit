package com.devcamp.customervisitapi.services;

import java.util.ArrayList;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.models.Visit;

@Service
public class VisitService extends CustomerService {
    Visit visit1 = new Visit(customer1.getName(), new Date(),100000,100000);
    Visit visit2 = new Visit(customer2.getName(), new Date(),200000, 300000);
    Visit visit3 = new Visit(customer3.getName(), new Date(),300000,4000000);
    public ArrayList<Visit> getAllVisits(){
        ArrayList<Visit> visitList = new ArrayList<>();
        visitList.add(visit1);
        visitList.add(visit2);
        visitList.add(visit3);
        return visitList;

    }
}
