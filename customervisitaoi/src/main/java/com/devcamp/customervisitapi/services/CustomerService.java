package com.devcamp.customervisitapi.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.customervisitapi.models.Customer;

@Service
public class CustomerService {
    Customer customer1 = new Customer("Huong");
    Customer customer2 = new Customer("Katty");
    Customer customer3 = new Customer("Minzy");

    public ArrayList<Customer> getAllCustomers(){
        ArrayList<Customer> customerList = new ArrayList<>();
        customerList.add(customer1);
        customerList.add(customer2);
        customerList.add(customer3);
        return customerList; 
    }
    
}
