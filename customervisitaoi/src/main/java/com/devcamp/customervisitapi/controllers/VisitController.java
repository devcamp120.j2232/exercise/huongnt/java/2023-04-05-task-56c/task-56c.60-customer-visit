package com.devcamp.customervisitapi.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.customervisitapi.models.Visit;
import com.devcamp.customervisitapi.services.VisitService;

@RestController
@CrossOrigin
@RequestMapping("/api")

public class VisitController {

    @Autowired
    VisitService visitService;
    @GetMapping("/visits")
    public ArrayList<Visit> getAllVisitsApi(){
        return visitService.getAllVisits();
    }

    
}
